import {useEffect, useState} from "react";

const TIMEOUT = 100;
const QuestionTimer = ({timeout, onTimeout}) => {
    const [remainingTime, setRemainingTime] = useState(timeout)

    useEffect(() => {
        console.log("Timer started")
        const timer = setTimeout(onTimeout, timeout);
        return () => {
            console.log("Timer cleared")
            clearTimeout(timer);
        }
    }, [])

    useEffect(() => {
        const interval = setInterval(() => setRemainingTime(prevRemainingTime => prevRemainingTime - TIMEOUT), TIMEOUT);
        return () => clearTimeout(interval)
    }, []);

    return <progress id="question-time" max={timeout} value={remainingTime}></progress>;
}

export default QuestionTimer;

import quizCompleteImg from "../assets/quiz-complete.png";

export default function QuizSummary() {
    return <div id="summary">
        <img src={quizCompleteImg} alt="Quiz summery"/>
        <h2>Quiz Completed!</h2>
    </div>
}

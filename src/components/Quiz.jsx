import {useCallback, useState} from "react";
import QUESTIONS from "./questions";
import QuizSummary from "./QuizSummery.jsx";
import QuestionTimer from "./QuestionTimer.jsx";

const TIMEOUT = 10000;

const ANS_STATUS = {
    EMPTY: '',
    ANSWERED: 'answered',
    CORRECT: 'correct',
    WRONG: 'wrong'
}

const Quiz = () => {
    // Current selected answer
    const [userAnswers, setUserAnswers] = useState([]);
    // Current selected answer status
    const [answerState, setAnswerState] = useState(ANS_STATUS.EMPTY);
    const activeQuestionIndex = answerState === ANS_STATUS.EMPTY ? userAnswers.length : userAnswers.length - 1;
    // Check if the quiz questions reaches the end
    const isQuizComplete = activeQuestionIndex === QUESTIONS.length;

    const onAnsSelect = useCallback(selectedAns => {
        setAnswerState(ANS_STATUS.ANSWERED);
        setUserAnswers(prevAns => [...prevAns, selectedAns]);
        setTimeout(() => {
            if (selectedAns === QUESTIONS[activeQuestionIndex].answers[0]) {
                setAnswerState(ANS_STATUS.CORRECT);
            } else {
                setAnswerState(ANS_STATUS.WRONG);
            }
            setTimeout(() => {
                setAnswerState(ANS_STATUS.EMPTY);
            }, 2000);
        }, 1000);
    }, [activeQuestionIndex]);

    const onTimeout = useCallback(() => onAnsSelect(null), [onAnsSelect]);

    if (isQuizComplete) {
        return <QuizSummary/>
    }

    const {text, answers, id} = QUESTIONS[activeQuestionIndex];
    const shuffledAns = [...answers];
    shuffledAns.sort(() => Math.random() - 0.5);

    return (
        <div id="quiz">
            <div id="question">
                <h2>{text}</h2>
                <QuestionTimer key={id} timeout={TIMEOUT} onTimeout={onTimeout}/>
                <ul id="answers">
                    {shuffledAns.map((answer) => {
                        let cssClass = ANS_STATUS.EMPTY;
                        const isSelected = userAnswers[userAnswers.length - 1] === answer;
                        if (isSelected) {
                            if (answerState === ANS_STATUS.ANSWERED) {
                                cssClass = 'selected';
                            }

                            if (answerState === ANS_STATUS.CORRECT || answerState === ANS_STATUS.WRONG) {
                                cssClass = answerState;
                            }
                        }
                        return (
                            <li key={answer} className="answer">
                                <button className={cssClass} onClick={() => onAnsSelect(answer)}>{answer}</button>
                            </li>
                        )
                    })}
                </ul>
            </div>
        </div>
    );
};

export default Quiz;
